Source: opencamlib
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Kurt Kremitzki <kurt@kwk.systems>
Section: science
Priority: optional
Build-Depends: cmake,
               debhelper (>= 11~),
               dh-python,
               libboost-graph-dev,
               libboost-python-dev,
               libboost-system-dev,
               python-dev,
Build-Depends-Indep: doxygen,
                     texlive-fonts-recommended,
                     texlive-latex-base,
                     texlive-latex-extra
Standards-Version: 4.2.1
Vcs-Browser: https://salsa.debian.org/science-team/opencamlib
Vcs-Git: https://salsa.debian.org/science-team/opencamlib.git
Homepage: http://www.anderswallin.net/cam/

Package: libopencamlib-2018.08
Architecture: any
Section: libs
Depends: ${misc:Depends},
         ${shlibs:Depends}
Suggests: opencamlib-doc
Description: C++ library for creating 3D toolpaths for CNC machines
 OpenCAMLib is a 3D CAM library in C++ with Python bindings. The main
 functionality it provides is axial and radial cutter-projection algorithms
 against polyhedral (triangulated) surfaces.

Package: libopencamlib-dev
Architecture: any
Section: libdevel
Depends: libopencamlib-2018.08 (<< ${binary:Version}+1~),
         libopencamlib-2018.08 (>= ${binary:Version}),
         ${misc:Depends}
Description: C++ library for creating 3D toolpaths for CNC machines - headers
 OpenCAMLib is a 3D CAM library in C++ with Python bindings. The main
 functionality it provides is axial and radial cutter-projection algorithms
 against polyhedral (triangulated) surfaces.
 .
 This package provides development headers for OpenCAMLib.

Package: python-opencamlib
Section: python
Architecture: any
Depends: ${misc:Depends},
         ${python:Depends},
         ${shlibs:Depends}
Provides: ${python:Provides}
Suggests: opencamlib-doc
Description: C++ library for creating 3D toolpaths for CNC machines - Python bindings
 OpenCAMLib is a 3D CAM library in C++ with Python bindings. The main
 functionality it provides is axial and radial cutter-projection algorithms
 against polyhedral (triangulated) surfaces.
 .
 This package contains the Python bindings for OpenCAMLib.

Package: opencamlib-doc
Section: doc
Architecture: all
Depends: ${misc:Depends}
Description: C++ library for creating 3D toolpaths for CNC machine - documentation
 OpenCAMLib is a 3D CAM library in C++ with Python bindings. The main
 functionality it provides is axial and radial cutter-projection algorithms
 against polyhedral (triangulated) surfaces.
 .
 This package contains documentation for OpenCAMLib.
